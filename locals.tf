locals {
  ec2_conf = var.ec2_conf
  vpc_conf = { for k, v in local.ec2_conf : k =>
    contains(keys(v), "vpc_id") ? {
      filters = [
        {
          name   = "vpc-id"
          values = [v.vpc_id]
        }
      ]
      } : contains(keys(v), "vpc_name") ? {
      filters = [
        {
          name   = "tag:Name"
          values = [v.vpc_name]
        }
      ]
    } : {}
  }
  subnet_conf = { for k, v in local.ec2_conf : k =>
    contains(keys(v), "subnet_id") ? {
      filters = [
        {
          name   = "subnet-id"
          values = [v.subnet_id]
        }
      ]
      } : contains(keys(v), "subnet_name") ? {
      filters = [
        {
          name   = "tag:Name"
          values = [v.subnet_name]
        }
      ]
    } : {}
  }

  
}
