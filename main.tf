
module "ssh-keypair" {
  source              = "./ssh-keypair"
  for_each            = var.ec2_conf
  create_ssh_key_pair = try(each.value.create_ssh_key_pair, var.create_ssh_key_pair)
  key_name            = try(each.value.create_ssh_key_pair, var.create_ssh_key_pair) ? each.value.key_name : var.key_name
  store_ssh_key_to_s3 = try(each.value.create_ssh_key_pair, var.create_ssh_key_pair) ? try(each.value.store_ssh_key_to_s3, var.store_ssh_key_to_s3) : var.store_ssh_key_to_s3
  storage_path        = try(each.value.store_ssh_key_to_s3, var.store_ssh_key_to_s3) ? each.value.key_storage_path : var.key_storage_path
  storage_bucket_name = try(each.value.store_ssh_key_to_s3, var.store_ssh_key_to_s3) ? each.value.ssh_key_bucket_name : var.ssh_key_bucket_name
  tags                = try(each.value.tags, var.tags)
}

module "vpc_details" {
  source      = "git::https://gitlab.com/terraswifft/platform/starter/vpc_details.git?ref=main"
  for_each    = var.ec2_conf
  vpc_id      = try(each.value.vpc_id, var.vpc_id)
  vpc_name    = try(each.value.vpc_name, var.vpc_name)
  subnet_id   = try(each.value.subnet_id, var.subnet_id)
  subnet_name = try(each.value.subnet_name, var.subnet_name)
}

module "ec2-instance" {
  source                               = "terraform-aws-modules/ec2-instance/aws"
  version                              = "5.1.0"
  for_each                             = var.ec2_conf
  name                                 = try(each.value.name, each.key, var.name)
  ami_ssm_parameter                    = try(each.value.ami_ssm_parameter, var.ami_ssm_parameter)
  ami                                  = each.value.ami
  ignore_ami_changes                   = try(each.value.ignore_ami_changes, var.ignore_ami_changes)
  associate_public_ip_address          = try(each.value.associate_public_ip_address, var.associate_public_ip_address)
  maintenance_options                  = try(each.value.maintenance_options, var.maintenance_options)
  availability_zone                    = try(each.value.availability_zone, var.availability_zone)
  capacity_reservation_specification   = try(each.value.capacity_reservation_specification, var.capacity_reservation_specification)
  cpu_credits                          = try(each.value.cpu_credits, var.cpu_credits)
  disable_api_termination              = try(each.value.disable_api_termination, var.disable_api_termination)
  ebs_block_device                     = try(each.value.ebs_block_device, var.ebs_block_device)
  ebs_optimized                        = try(each.value.ebs_optimized, var.ebs_optimized)
  enclave_options_enabled              = try(each.value.enclave_options_enabled, var.enclave_options_enabled)
  ephemeral_block_device               = try(each.value.ephemeral_block_device, var.ephemeral_block_device)
  get_password_data                    = try(each.value.get_password_data, var.get_password_data)
  hibernation                          = try(each.value.hibernation, var.hibernation)
  host_id                              = try(each.value.host_id, var.host_id)
  iam_instance_profile                 = try(each.value.iam_instance_profile, var.iam_instance_profile)
  instance_initiated_shutdown_behavior = try(each.value.instance_initiated_shutdown_behavior, var.instance_initiated_shutdown_behavior)
  instance_type                        = each.value.instance_type
  ipv6_address_count                   = try(each.value.ipv6_address_count, var.ipv6_address_count)
  ipv6_addresses                       = try(each.value.ipv6_addresses, var.ipv6_addresses)
  key_name                             = try(each.value.create_ssh_key_pair, var.create_ssh_key_pair) ? module.ssh-keypair[each.key].key_name : try(each.value.key_name, var.key_name)
  launch_template                      = try(each.value.launch_template, var.launch_template)
  metadata_options                     = try(each.value.metadata_options, var.metadata_options)
  monitoring                           = try(each.value.monitoring, var.monitoring)
  network_interface                    = try(each.value.network_interface, var.network_interface)
  placement_group                      = try(each.value.placement_group, var.placement_group)
  private_ip                           = try(each.value.private_ip, var.private_ip)
  root_block_device                    = try(each.value.root_block_device, var.root_block_device)
  secondary_private_ips                = try(each.value.secondary_private_ips, var.secondary_private_ips)
  source_dest_check                    = try(each.value.source_dest_check, var.source_dest_check)
  subnet_id                            = module.vpc_details[each.key].subnet_id
  tags                                 = try(each.value.tags, var.tags)
  tenancy                              = try(each.value.tenancy, var.tenancy)
  user_data                            = try(each.value.user_data, var.user_data)
  user_data_base64                     = try(each.value.user_data_base64, var.user_data_base64)
  user_data_replace_on_change          = try(each.value.user_data_replace_on_change, var.user_data_replace_on_change)
  volume_tags                          = try(each.value.volume_tags, var.volume_tags)
  enable_volume_tags                   = try(each.value.enable_volume_tags, var.enable_volume_tags)
  vpc_security_group_ids               = try(each.value.create_security_group, var.create_security_group) ? concat([module.security_group[each.key].security_group_id], try(each.value.vpc_security_group_ids, [])) : each.value.vpc_security_group_ids
  timeouts                             = try(each.value.timeouts, var.timeouts)
  cpu_options                          = try(each.value.cpu_options, var.cpu_options)
  cpu_core_count                       = try(each.value.cpu_core_count, var.cpu_core_count)
  cpu_threads_per_core                 = try(each.value.cpu_threads_per_core, var.cpu_threads_per_core)
  create_spot_instance                 = try(each.value.create_spot_instance, var.create_spot_instance)
  spot_price                           = try(each.value.spot_price, var.spot_price)
  spot_wait_for_fulfillment            = try(each.value.spot_wait_for_fulfillment, var.spot_wait_for_fulfillment)
  spot_type                            = try(each.value.spot_type, var.spot_type)
  spot_launch_group                    = try(each.value.spot_launch_group, var.spot_launch_group)
  spot_block_duration_minutes          = try(each.value.spot_block_duration_minutes, var.spot_block_duration_minutes)
  spot_instance_interruption_behavior  = try(each.value.spot_instance_interruption_behavior, var.spot_instance_interruption_behavior)
  spot_valid_until                     = try(each.value.spot_valid_until, var.spot_valid_until)
  spot_valid_from                      = try(each.value.spot_valid_from, var.spot_valid_from)
  disable_api_stop                     = try(each.value.disable_api_stop, var.disable_api_stop)
  create_iam_instance_profile          = try(each.value.create_iam_instance_profile, var.create_iam_instance_profile)
  iam_role_name                        = try(each.value.iam_role_name, var.iam_role_name)
  iam_role_use_name_prefix             = try(each.value.iam_role_use_name_prefix, var.iam_role_use_name_prefix)
  iam_role_path                        = try(each.value.iam_role_path, var.iam_role_path)
  iam_role_description                 = try(each.value.iam_role_description, var.iam_role_description)
  iam_role_permissions_boundary        = try(each.value.iam_role_permissions_boundary, var.iam_role_permissions_boundary)
  iam_role_policies                    = try(each.value.iam_role_policies, var.iam_role_policies)
  iam_role_tags                        = try(each.value.iam_role_tags, var.iam_role_tags)
}

module "security_group" {
  source                 = "./security_group"
  for_each               = var.ec2_conf
  create_security_group  = try(each.value.create_security_group, var.create_security_group)
  name                   = try(each.value.create_security_group, var.create_security_group) ? try(each.value.security_group_name, var.security_group_name) : var.security_group_name
  description            = try(each.value.create_security_group, var.create_security_group) ? try(each.value.security_group_description, var.security_group_description) : var.security_group_description
  vpc_id                 = module.vpc_details[each.key].vpc_id
  revoke_rules_on_delete = try(each.value.create_security_group, var.create_security_group) ? try(each.value.security_group_revoke_rules_on_delete, var.security_group_revoke_rules_on_delete) : var.security_group_revoke_rules_on_delete
  tags                   = try(each.value.tags, var.tags)
  ingress_rules          = try(each.value.create_security_group, var.create_security_group) ? try(each.value.security_group_ingress_rules, var.security_group_ingress_rules) : var.security_group_ingress_rules
  egress_rules           = try(each.value.create_security_group, var.create_security_group) ? try(each.value.security_group_egress_rules, var.security_group_egress_rules) : var.security_group_egress_rules
}


module "route53_records" {
  source                = "./route53-record"
  for_each              = var.ec2_conf
  create_route53_record = try(each.value.create_route53_record, var.create_route53_record)
  name                  = try(each.value.create_route53_record, var.create_route53_record) ? each.value.route53_record_name : var.route53_record_name
  zone_name             = try(each.value.create_route53_record, var.create_route53_record) ? each.value.route53_zone_name : var.route53_zone_name
  private_zone          = try(each.value.create_route53_record, var.create_route53_record) ? try(each.value.is_zone_private, var.is_zone_private) : var.is_zone_private
  ttl                   = try(each.value.create_route53_record, var.create_route53_record) ? each.value.route53_record_ttl : var.route53_record_ttl
  records               = try(each.value.create_route53_record, var.create_route53_record) ? try(each.value.is_zone_private, var.is_zone_private) ? [module.ec2-instance[each.key].private_ip] : [module.ec2-instance[each.key].public_ip] : var.route53_records
}

module "remote_exec" {
  source                      = "./remote-exec"
  for_each                    = var.ec2_conf
  remote_exec_code            = try(each.value.create_ssh_key_pair) ? try(each.value.remote_exec_code, var.remote_exec_code) : var.remote_exec_code
  remote_exec_using_public_ip = try(each.value.create_ssh_key_pair) ? try(each.value.remote_exec_using_public_ip, var.remote_exec_using_public_ip) : var.remote_exec_using_public_ip
  hold_remote_exec_for_sec    = try(each.value.create_ssh_key_pair) ? try(each.value.hold_remote_exec_for_sec, var.hold_remote_exec_for_sec) : var.hold_remote_exec_for_sec
  instance_private_ip         = try(each.value.create_ssh_key_pair) ? try(module.ec2-instance[each.key].private_ip, var.instance_private_ip) : var.instance_private_ip
  instance_public_ip          = try(each.value.create_ssh_key_pair) ? try(module.ec2-instance[each.key].public_ip, var.instance_public_ip) : var.instance_public_ip
  ssh_user                    = try(each.value.create_ssh_key_pair) ? try(each.value.ssh_user, var.ssh_user) : var.ssh_user
  ssh_private_key             = try(each.value.create_ssh_key_pair) ? try(module.ssh-keypair[each.key].private_key, var.ssh_private_key) : var.ssh_private_key
  ssh_connection_timeout      = try(each.value.create_ssh_key_pair) ? try(each.value.ssh_connection_timeout, var.ssh_connection_timeout) : var.ssh_connection_timeout
  ssh_script_path             = try(each.value.create_ssh_key_pair) ? try(each.value.ssh_script_path, var.ssh_script_path) : var.ssh_script_path
}
