locals {
  inline_code = var.hold_remote_exec_for_sec > 0 ? concat(["sleep ${var.hold_remote_exec_for_sec}"], var.remote_exec_code) : var.remote_exec_code
    remote_exec_code_hash = md5(join("", local.inline_code))
}
