resource "null_resource" "remote_exec" {
  count = length(var.remote_exec_code) > 0 ? 1 : 0

  triggers = {
    remote_exec_code_hash = local.remote_exec_code_hash
  }

  provisioner "remote-exec" {
    connection {
      type        = "ssh"
      host        = var.remote_exec_using_public_ip ? var.instance_public_ip : var.instance_private_ip
      user        = var.ssh_user
      private_key = var.ssh_private_key
      timeout     = var.ssh_connection_timeout
      script_path = var.ssh_script_path
    }
    inline = local.inline_code
  }

}
