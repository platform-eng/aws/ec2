variable "remote_exec_code" {
  type    = list(string)
  default = []
}

variable "remote_exec_using_public_ip" {
  type    = bool
  default = true
}

variable "hold_remote_exec_for_sec" {
  type    = number
  default = 0
}

variable "instance_private_ip" {
  type    = string
  default = null
}

variable "instance_public_ip" {
  type    = string
  default = null
}

variable "ssh_user" {
  type    = string
  default = "ubuntu"
}

variable "ssh_private_key" {
  type    = string
  default = null
}

variable "ssh_connection_timeout" {
  type    = string
  default = null
}

variable "ssh_script_path" {
  type    = string
  default = null
}
