data "aws_route53_zone" "this" {
  count        = var.create_route53_record ? 1 : 0
  name         = var.zone_name
  private_zone = var.private_zone
}

resource "aws_route53_record" "this" {
  count   = var.create_route53_record ? 1 : 0
  name    = var.name
  zone_id = data.aws_route53_zone.this.0.zone_id
  type    = "A"
  ttl     = var.ttl
  records = var.records
}
