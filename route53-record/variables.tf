variable "name" {
  type = string
}

variable "ttl" {
  type = number
}

variable "records" {
  type = list(any)
}

variable "zone_name" {
  type = string
}

variable "private_zone" {
  type = bool
}

variable "create_route53_record" {
  type = bool
}
