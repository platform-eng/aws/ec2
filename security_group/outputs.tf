output "security_group_name" {
  value = try(aws_security_group.this.0.name,null)
}

output "security_group_id" {
  value = try(aws_security_group.this.0.id,null)
}
