resource "tls_private_key" "this" {
  count     = var.create_ssh_key_pair ? 1 : 0
  algorithm = "RSA"
  rsa_bits  = 4096
}

# locals {
#   key_name = "env:/${var.environment}/ssh-keys/${var.environment}-${var.product}-${var.use_case}-keypair.pem"
# }

resource "aws_s3_object" "this" {
  count    = var.create_ssh_key_pair ?  var.store_ssh_key_to_s3 ? 1 : 0 : 0
  key      = "${var.storage_path}/${var.key_name}.pem"
  bucket   = var.storage_bucket_name
  content  = tls_private_key.this.0.private_key_pem
  tags     = var.tags
  metadata = {}

}
resource "aws_key_pair" "this" {
  count      = var.create_ssh_key_pair ? 1 : 0
  key_name   = var.key_name
  public_key = tls_private_key.this.0.public_key_openssh
  tags       = var.tags
}

