output "public_key" {
  value     = try(tls_private_key.this.0.public_key_openssh, null)
  sensitive = true
}

output "private_key" {
  value     = try(tls_private_key.this.0.private_key_pem, null)
  sensitive = true
}

output "storage_path_of_keypair" {
  value = try("s3://${aws_s3_object.this.0.bucket}/${aws_s3_object.this.0.key}", null)
}

output "key_name" {
  value = try(aws_key_pair.this.0.key_name, null)
}
