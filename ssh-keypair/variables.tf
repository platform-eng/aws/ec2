variable "create_ssh_key_pair" {
  type    = bool
  default = false
}
variable "key_name" {
  type    = string
  default = ""
}

variable "store_ssh_key_to_s3" {
  type = bool
  default = false
}
variable "storage_bucket_name" {
  type    = string
  default = ""
}
variable "storage_path" {
  type    = string
  default = "/"
}

variable "tags" {
  type    = any
  default = {}
}


